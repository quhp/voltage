/*
    2012 QUHP Kora
*/

#include "IOSerial.h"
#include "mbed.h"

// functions
IOSerial::IOSerial(PinName txrx) : pin(txrx){
    // 115200bps, 8bit, start 1bit, stop 1bit, parity even
    // one bit takes 8.68us
}

void IOSerial::putc(unsigned char data)
{   __disable_irq();
    unsigned char i, num, parity;
    
    pin.output();
//    pin.mode(OpenDrain);
    
    // even?
    num = 0;
    for(i=0; i<8; i++)
        if( (data&(0x01<<i))>>i )
            num++;
    if( num % 2 )
        parity = 1; // if odd then parity bit = 1
    else
        parity = 0; // if even then parity bit = 0
    
    // start bit
    pin = 0;
    wait_us(7.48);
    // data bits
    for(i=0; i<8; i++){
        pin = ( (data&(0x01<<i))>>i );
        wait_us(7.48);
    }
    // parity bit
    pin = parity;
    wait_us(7.48);
    // stop bit
    pin = 1;
    wait_us(7.48);

    // end
    pin = 1;
    __enable_irq();
}

unsigned char IOSerial::getc(void)
{
    int i, j=0;
    unsigned char data;
    
    pin.input();
    pin.mode(PullNone);
    
    // wait for start bit
    for(i=0; i<1000000; i++){
        if(pin==0){
            if(j<5){
                j++;
            }else{
                wait_us(4);
                wait_us(8);
                break;
            }
        }else{
            j = 0;
        }
    }
    
    // receiving data bits
    data = 0x00;
    for(i=0; i<8; i++){
        data |= (pin<<i);
        wait_us(8);
    }
    
    // wait for stop bit
    wait_us(4);
    wait_us(8);
    wait_us(8);
    for(i=0; i<1000000; i++){
        if(pin==1){
            break;
        }
    }
    
    // return data
    return data;
}


