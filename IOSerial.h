/*
    2012 QUHP Kora
*/

#ifndef IOSERIAL_H
#define IOSERIAL_H

#include "mbed.h"

class IOSerial{
public:
    IOSerial(PinName txrx);
    void putc(unsigned char);
    unsigned char getc(void);
    
private:
    DigitalInOut pin;
    Timer tm;
};

#endif
