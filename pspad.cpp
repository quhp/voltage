/*
 * pspad.c
 *
 *  Created on: 2012/12/21
 *      Author: KORA
 */

#include "mbed.h"
#include "pspad.h"

//----------------------------------------------------------
// PS PAD
//----------------------------------------------------------
#define PS_DAT    datpin        // こいつだけ要プルアップ
#define PS_CMD_H  cmdpin = 1
#define PS_CMD_L  cmdpin = 0
#define PS_SEL_H  selpin = 1
#define PS_SEL_L  selpin = 0
#define PS_CLK_H  clkpin = 1
#define PS_CLK_L  clkpin = 0

//----------------------------------------------------------
// 動作周波数
//----------------------------------------------------------
#define PS_FREQ    100              // パッドの動作周波数    (kHz)
                                    // タイマはこの２倍の周波数で動かす
                                    // 本来250kHzだが変更しても問題ない

#define TM_PERI    (500/PS_FREQ)    // タイマの１クロックに要する時間(us)
                                    // タイマの２クロックをパッドの１クロックとしている
const double scale=500/PS_FREQ;
//----------------------------------------------------------
// constructor
//----------------------------------------------------------
Pspad::Pspad(PinName dat, PinName cmd, PinName sel, PinName clk) : datpin(dat), cmdpin(cmd), selpin(sel), clkpin(clk)
{

    // GPIOの出力値を設定する
    PS_CMD_H;               // CMD を1に
    PS_CLK_H;               // CLK を1に
    PS_SEL_H;               // SEL を1に
    
    // タイマの設定
//    tm.reset();
}

//----------------------------------------------------------
// プレステ用ゲームパッドとの１バイトの通信
// 引数
//  int send : 送信データ（0～255）
// 戻り値
//  int 受信データ（0～255）
//----------------------------------------------------------
uint8_t Pspad::PsComm( uint8_t send_data )
{
    uint8_t i;
    uint8_t recv_data = 0;
    uint8_t sendb_data[8] = {0,0,0,0,0,0,0,0};
    uint8_t recvb_data[8] = {0,0,0,0,0,0,0,0};
    int count=0;
    int kan;
//    kan=TM_PERI-4;
    // カウント開始
//    tm.start();
for(i=0;i<8;i++){
	sendb_data[i]=(send_data & 0x01);
	send_data >>= 1;
}

    for(i=0; i<8; i++)
    {

        // 送信データ
//        if(sendb_data[i] & 0x01){
//            cmdpin=1;
//        }else{
//        	cmdpin=0;
//        }
    	cmdpin=sendb_data[i];
        // クロックをLOWに
        PS_CLK_L;
        // 待機
//        while(tm.read_us()/TM_PERI <= 2*i-count*1);
        wait_us(TM_PERI);

        recvb_data[i] = (PS_DAT);
        // クロックをHIGHに
        PS_CLK_H;
        // 受信データ
//        recvb_data[i] = (PS_DAT);
        // 待機
        wait_us(TM_PERI);
//        while(tm.read_us()/TM_PERI <= 2*i+1-count*1);
        // 送信データを１ビットずらす

    }

    // CMDをHIGHに戻す
    PS_CMD_H;

    // ５クロックほど待機
//    while(tm.read_us()/TM_PERI <= 2*8+2*5);
     wait_us(TM_PERI*5);
    // カウント停止 及び リセット
//    tm.stop();
//    tm.reset();
     for(i=0;i<8;i++){

     	 recv_data |= (recvb_data[i]<<i);
     }
    return recv_data;
}

//----------------------------------------------------------
// プレステ用ゲームパッドからのデータの取得
// 変数
//  uint32_t key      ボタンの押下状態をbitmapで返す。
//  int8_t   right_x  -128～127 の値を返す。アナログスティック中立時は０
//  int8_t   right_y  -128～127 の値を返す。アナログスティック中立時は０
//  int8_t   left_x   -128～127 の値を返す。アナログスティック中立時は０
//  int8_t   left_y   -128～127 の値を返す。アナログスティック中立時は０
//----------------------------------------------------------
void Pspad::PsRead(void)
{
    uint8_t i, len;
    uint8_t rcv[20];

    key = 0;
    right_x = 0;  // アナログスティック中立
    right_y = 0;  // アナログスティック中立
    left_x  = 0;  // アナログスティック中立
    left_y  = 0;  // アナログスティック中立

    // SELをLOWに
    PS_SEL_L;

    // 少し待機
//    tm.start();
//    while(tm.read_us() <=  40);      // 40usでも動く模様
    wait_us(40);
//    tm.stop();
//    tm.reset();

    // 通信開始
    rcv[0] = PsComm(0x01);          // PADはコマンド'01h'を検出して動作を開始する
    rcv[1] = PsComm(0x42);          // 受信データの下位４ビットが転送バイト数の半分の数を表す
    len = (rcv[1] & 0x03) * 2;      // 転送バイト数を求める
    for(i=0;i<len+1;i++){
        rcv[i+2] = PsComm(0x00);    // PADの状態を受信
    }

    // SELをHIGHに
    PS_SEL_H;

    // PADの状態を返す
    
    key = ((rcv[3]<<8) | rcv[4]) ^ 0xFFFF;
    if(len >= 6){
        right_x = rcv[5] - 128;
        right_y = rcv[6] - 128;
        left_x  = rcv[7] - 128;
        left_y  = rcv[8] - 128;
    }
    convert_button();
}
void Pspad::convert_button(void){
	uint32_t padkey=key;
	BUTTON.BIT.L2=(padkey & 0x01);
	padkey >>=(1);
	BUTTON.BIT.R2=(padkey & 0x01);
	padkey >>=(1);
	BUTTON.BIT.L1=(padkey & 0x01);
	padkey >>=(1);
	BUTTON.BIT.R1=(padkey & 0x01);
	padkey >>=(1);
	BUTTON.BIT.TRINANGLE=(padkey & 0x01);
	padkey >>=(1);
	BUTTON.BIT.CIRCLE=(padkey & 0x01);
	padkey >>=(1);
	BUTTON.BIT.CROSS=(padkey & 0x01);
	padkey >>=(1);
	BUTTON.BIT.SQUARE=(padkey & 0x01);
	padkey >>=(1);
	BUTTON.BIT.SELECT=(padkey & 0x01);
	padkey >>=(1);
//	BUTTON.BIT.NC=(padkey & 0x01);
	padkey >>=(1);
//	BUTTON.BIT.NC=(padkey & 0x01);
	padkey >>=(1);
	BUTTON.BIT.START=(padkey & 0x01);
	padkey >>=(1);
	BUTTON.BIT.UP=(padkey & 0x01);
	padkey >>=(1);
	BUTTON.BIT.RIGHT=(padkey & 0x01);
	padkey >>=(1);
	BUTTON.BIT.DOWN=(padkey & 0x01);
	padkey >>=(1);
	BUTTON.BIT.LEFT=(padkey & 0x01);
}
