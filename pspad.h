/*
 * pspad.h
 *
 *  Created on: 2012/12/21
 *      Author: USER
 */

#ifndef PSPAD_H_
#define PSPAD_H_

//----------------------------------------------------------
// PS PAD キーシンボル定義
//----------------------------------------------------------
#define B_L2        0x0001  // L2
#define B_R2        0x0002  // R2
#define B_L1        0x0004  // L1
#define B_R1        0x0008  // R1
#define B_SANKAKU   0x0010  // △
#define B_MARU      0x0020  // ◯
#define B_BATU      0x0040  // ☓
#define B_SIKAKU    0x0080  // □
#define B_SELECT    0x0100  // SELECT
#define B_L3        0x0200  // 左アナログPUSH
#define B_R3        0x0400  // 右アナログPUSH
#define B_START     0x0800  // START
#define B_UE        0x1000  // ↑
#define B_MIGI      0x2000  // →
#define B_SITA      0x4000  // ↓
#define B_HIDARI    0x8000  // ←

//-------------------------------------------------------------------------
// Class
//-------------------------------------------------------------------------
class Pspad
{

    public:
    union {
        uint32_t WORD;
        struct {
            unsigned char L2       :1;
            unsigned char R2       :1;
            unsigned char L1       :1;
            unsigned char R1       :1;

            unsigned char TRINANGLE  :1;
            unsigned char CIRCLE    :1;
            unsigned char CROSS     :1;
            unsigned char SQUARE   :1;

            unsigned char SELECT   :1;
            unsigned char L3       :1;
            unsigned char R3       :1;
            unsigned char START    :1;

            unsigned char UP       :1;
            unsigned char RIGHT    :1;
            unsigned char DOWN     :1;
            unsigned char LEFT     :1;
        } BIT;
    } BUTTON;

        Pspad(PinName dat, PinName cmd, PinName sel, PinName clk);
        void PsRead(void);
        uint32_t key;
        int8_t right_x, right_y, left_x, left_y;
        void convert_button();
    private:
        uint8_t PsComm(uint8_t send_data);
//        Timer tm;
        DigitalIn  datpin;
        DigitalOut cmdpin;
        DigitalOut selpin;
        DigitalOut clkpin;

};


#endif /* PSPAD_H_ */
