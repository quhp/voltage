#include"savo_cont.h"


#include "pspad.h"
Serial CT(USBTX,USBRX);
IOSerial LUO(p22);
IOSerial RUO(p24);
IOSerial RFO (p26);
IOSerial LFO (p30);
Timer MOVE_TIME;

//Serial pc(USBTX,USBRX);
//DigitalOut LEDT(p20);
//AnalogIn JOUGE(p17);
//AnalogIn SAYUU(p16);
//AnalogIn ZENGO(p15);
//AnalogIn Y(p19);
//AnalogIn X(p20);
Pspad  PS(p6, p7, p5, p8);

double hohaba_a;//歩幅保存用
int ZMP_roop=1;//現状では常に1
const long double pi=3.141592654;//円周率
long int walk_roop=0;//歩行状態監視用　0->1歩行はじめ　1->0歩行終わり　1->1歩行中 walk() とwalk_resetで使う

//------------------MOVE_SAVO現在角度確認用変数
int LUS[6]={4834,4834,4834,6000,4834,4834};
int RUS[6]={4834,4834,4834,7000,4834,4834};
int RFS[6]={4834,4834,4834,8000,4834,4834};
int LFS[6]={4834,4834,4834,9000,4834,4834};
//-------------------------------------
double offset=-2;//歩行調整用
double buf_XR=0,buf_XL=0;
//ホームポジション設定用--------------------------------------------
int home_pos[4][6]={{7170,7000,7500,8500,7500,7500},
                    {7500,8000,7500,8500,7500,7500},
                    {7400,7500-335,7500+335,7500,/*ここまで*/7500,7500},
                    {7500,7500,7500+335,7500+335,7500-50,/*ここまで*/7500}};
//--------------------------------------------------------------------

int free_pos[4][6]={{0,0,0,0,0,0},
                    {0,0,0,0,0,0},
                    {0,0,0,0,/*ここまで*/0,0},
                    {0,0,0,0,0,/*ここまで*/0},};//フリーポジション設定用//
int home_pos2[4][6]={{home_pos[0][0],home_pos[0][1],home_pos[0][2],home_pos[0][3],home_pos[0][4],home_pos[0][5]},
                    {home_pos[1][0],home_pos[1][1],home_pos[1][2],home_pos[1][3],home_pos[1][4],home_pos[1][5]},
                    {home_pos[2][0],home_pos[2][1],home_pos[2][2],home_pos[2][3],home_pos[2][4],home_pos[2][5]},
                    {home_pos[3][0],home_pos[3][1],home_pos[3][2],home_pos[3][3],home_pos[3][4],home_pos[3][5]},};//ホームポジション保存用


int now_pos[4][6]={{home_pos[0][0],home_pos[0][1],home_pos[0][2],home_pos[0][3],home_pos[0][4],home_pos[0][5]},
        {home_pos[1][0],home_pos[1][1],home_pos[1][2],home_pos[1][3],home_pos[1][4],home_pos[1][5]},
        {home_pos[2][0],home_pos[2][1],home_pos[2][2],home_pos[2][3],home_pos[2][4],home_pos[2][5]},
        {home_pos[3][0],home_pos[3][1],home_pos[3][2],home_pos[3][3],home_pos[3][4],home_pos[3][5]},};//サーボ角度保存用

//-----------------------歩行ステータス決定-----------------------
static double asiage =5;//あしを上げる高さ
 double period=0.43;// 0.47;//0.5//周期決定用
 double buf_period=period;//周期保存用
static double GRAV=980;// 重力
static double WALK_HEIGHT=26.5;//歩行高さ
//---------------歩行ステータス決定
double buf_walkheight=WALK_HEIGHT;
static double CENT_G =WALK_HEIGHT;
double hohaba=0;
double hohaba2=0;

double hohabaMAX=10;//歩幅限界値設定用
double Ymax =7;     //ホームポジション決定用
double walk_Ymax=6;//歩行横幅決定用

double sYmax=Ymax,sYmax2=Ymax;
double aYmax=Ymax,aYmax2=Ymax;
int QU;//反転用

/*---------------------歩行計算用係数宣言--------------------------------------*/
double _CYL2,_CYL1;//左足歩行Y方向係数
double _CYR2,_CYR1;//右足歩行Y方向係数
double _CXL1,_CXL2;//左足歩行X方向係数
double _CXR1,_CXR2;//右足歩行X方向係数
double _CRf11,_CRf12,_CRf13,_CRf14,_CRf21,_CRf22,_CRf23,_CRf24;//左足Y方向座標計算係数(足が浮いているとき)
double _CLf11,_CLf12,_CLf13,_CLf14,_CLf21,_CLf22,_CLf23,_CLf24;//左足Y方向座標計算係数(足が浮いているとき)
double _CRXf11,_CRXf12,_CRXf13,_CRXf14,_CRXf21,_CRXf22,_CRXf23,_CRXf24;//左足X方向座標計算係数(足が浮いているとき)
double _CLXf11,_CLXf12,_CLXf13,_CLXf14,_CLXf21,_CLXf22,_CLXf23,_CLXf24;//左足X方向座標計算係数(足が浮いているとき)
double CZ1,CZ2,CZ3,CZ4;//右足、左足Z方向座標計算係数(足が浮いているとき)
double CZE1,CZE2,CZE3,CZE4;//右足、左足Z方向歩行終了時座標計算係数(足が浮いているとき)
double Cs1_buf,Cs2_buf, Ca1_buf,Ca2_buf;//1ループ前の歩行情報保存用　s=start a=after
double _Cab_speed_T,_Cab_point_T,_Cs_point_T,_Cs_speed_T;//計算用変数　ZMP の数式から終点の速さを求める s=start
double _Ca_point_0,_Ca_speed_0,_Ca_point_T,_Ca_speed_T;//計算用変数　ZMP の数式から終点の速さを求める  a=after
double Vsn,_CYL2n,_CYL1n,_Csn_point_0,_Csn_speed_0,_CLn1,_CLn2;; //次のループにおける足の挙動の予想

/*---------------------歩行計算用係数宣言--------------------------------------*/
//--------------------サーボコントロール用プログラム----------------------------------------------------//
void setSpeed(int port,unsigned char id,int sp)
{
    unsigned char tx[3]; //send date
   
    int i;
    
    tx[0] = 0xC0 | id;     
    tx[1] = 0x02;
    tx[2] = 0x00 | sp;
    // LEFT ARM_SET_SP
    if(port == 0){
    for(i=0;i<3;i++){ 
    LUO.putc(tx[i]);
    }
    }
    //RIGHT ARM_SET_SP
    else if(port == 1){
    for(i=0;i<3;i++){
    RUO.putc(tx[i]);
    }
    }
    //RIGHT LEG_SET_SP
    else if(port == 2){
    for(i=0;i<3;i++){
    RFO.putc(tx[i]);
    }
    }
    //LEFT LEG_SET_SP
    else if(port == 3){
    for(i=0;i<3;i++){
    LFO.putc(tx[i]);
    }
    }
    else;
}

void setPosition(int port,unsigned char id,int pos)
{
    unsigned char tx[3]; //send date
    unsigned char rx[3]; //return date
   
    int i;
    
    tx[0] = 0x80 | id;     
    tx[1] = (unsigned char)(pos >> 7 & 0x7F);
    tx[2] = (unsigned char)(pos & 0x7F);
    if( pos<=11500 && pos>= 3500  ){


    __disable_irq();
    //LEFT_ARM_SET_POS
    if(port == 0){
    for(i=0;i<3;i++){
    LUO.putc(tx[i]);
    }
    } 
    
    //RIGHT_ARM_SET_POS
    else if(port == 1){
    for(i=0;i<3;i++){
    RUO.putc(tx[i]);
    }
    }

    //RIGHT_LEG_SET_POS
    else if(port == 2){
    for(i=0;i<3;i++){
    RFO.putc(tx[i]);
    }
    }
    
    //LEFT_LEG_SET_POS
    else if(port ==3){
    for(i=0;i<3;i++){
    LFO.putc(tx[i]);
    }
    }
    else;
    __enable_irq();
    }
    else;
    }

void setPosition_free(int port,unsigned char id,int pos)
{
    unsigned char tx[3]; //send date
    unsigned char rx[3]; //return date

    int i;

    tx[0] = 0x80 | id;
    tx[1] = (unsigned char)(pos >> 7 & 0x7F);
    tx[2] = (unsigned char)(pos & 0x7F);



    __disable_irq();
    //LEFT_ARM_SET_POS
    if(port == 0){
    	for(i=0;i<3;i++){
    		LUO.putc(tx[i]);
    	}
    }

    //RIGHT_ARM_SET_POS
    else if(port == 1){
    	for(i=0;i<3;i++){
    		RUO.putc(tx[i]);
    	}
    }

    //RIGHT_LEG_SET_POS
    else if(port == 2){
    	for(i=0;i<3;i++){
    		RFO.putc(tx[i]);
    	}
    }

    //LEFT_LEG_SET_POS
    else if(port ==3){
    	for(i=0;i<3;i++){
    		LFO.putc(tx[i]);
    	}
    }
    else;
    __enable_irq();
    }
//------------サーボコントロール用係数----------------------------
   

/*----------------座標計算用 （X,Y,Z)-> 関節角度-----------------------------*/
double SITAL1[4],SITAL2[4],SITAL3[4],SITAL4[4],SITAL5[4],SITAR1[4],SITAR2[4],SITAR3[4],SITAR4[4],SITAR5[4];//幾何学計算結果保存用
/*------------------幾何学計算用機体変数---------------------------------------*/
//単位　cgs単位系
static long double foot_length=10.2;
static long double foot_length_G=foot_length*0.5;
static long double foot_length_G2=foot_length*0.5;
static long double foot_double=foot_length*foot_length;
static double foot_k1=1.7,foot_k2=1.75,foot_h=2.2;
/*------------------幾何学計算用機体変数---------------------------------*/

double kikagaku(double xL,double yL,double xR,double yR){
	long double sita1R,sita2R,sita1L,sita2L;
	long double calL1,calL2,calR1,calR2;

//-----------foot_cal---------------------------------------------------------------
	yL=yL-foot_k1-foot_k2-foot_h;
    calL1=(xL*xL+yL*yL)/(2*foot_length*sqrt(xL*xL+yL*yL));
    calL2=((xL*xL+yL*yL)-2*foot_double)/(2*foot_double);

    sita1L=asin(calL1)-asin(xL/(sqrt(xL*xL+yL*yL)));
	sita2L=acos(calL2);
	SITAL2[ZMP_roop]=pi/2-sita1L;
	SITAL3[ZMP_roop]=pi/2-(pi-sita1L-sita2L);
//            end of foot_L--------------------------------------------------
	xR=xR;
	yR=yR-foot_k1-foot_k2-foot_h;
    calR1=(xR*xR+yR*yR)/(2*foot_length*sqrt(xR*xR+yR*yR));
    calR2=((xR*xR+yR*yR)-2*foot_double)/(2*foot_double);

    sita1R=asin(calR1)-asin(xR/(sqrt(xR*xR+yR*yR)));
	sita2R=acos(calR2);
	SITAR2[ZMP_roop]=pi/2-sita1R;
	SITAR3[ZMP_roop]=pi/2-(pi-sita1R-sita2R);
//    ----------------------------------------       end of foot_R

}

double RL[4],RR[4];//幾何学計算保存用
double kikagaku2(double xL,double zL,double xR,double zR){
	double rL,rR;
	double sitaL,sitaR,sitaL2,sitaR2;
	double L;
	double sinL,sinR;
	//left_LEG
	xL=xL-4.6;
	xR=xR-4.6;

//------------------------------------------------------
    sitaL=atan(xL/(zL-2.2));
    sinL=sin(sitaL);
    RL[ZMP_roop]=xL/sinL;
	SITAL1[ZMP_roop]=sitaL;
	SITAL4[ZMP_roop]=-sitaL;
//-------------------------------------------------------------------------
	sitaR=atan(xR/(zR-2.2));
	sinR=sin(sitaR);
	RR[ZMP_roop]=xR/sinR;
	SITAR1[ZMP_roop]=sitaR;
	SITAR4[ZMP_roop]=-sitaR;
//---------------------------------------------------------------------
//pc.printf("%f\n\r",RL[ZMP_roop]);
//pc.printf("%f\n\r",RR[ZMP_roop]);
}
/*----------------座標計算用 （X,Y,Z)-> 関節角度-----------------------------*/



//-----------------------------通常歩行座標決定用プログラム-------------------------------------------------
double ZMP_SIDE_YRR[4],ZMP_SIDE_YLL[4],ZMP_SIDE_ZRR[4],ZMP_SIDE_ZLL[4],ZMP_FORWARD_XLL,ZMP_FORWARD_XRR;//ZMP計算結果保存用

void ZMP_SIDE(double TIME){//ロボットの正面に対して横方向のZMP座標計算Y
double YL,YR;

if(TIME<=period/2){
	//-----------------------start--------------------------------
     if(TIME<=period/4){
     YL=_CYL1*exp(sqrt(GRAV/CENT_G)*TIME)+_CYL2*exp(-sqrt(GRAV/CENT_G)*TIME);
     YR=_CRf11+_CRf12*(TIME)+_CRf13*(TIME)*(TIME)+_CRf14*(TIME)*(TIME)*(TIME);

     ZMP_SIDE_YRR[ZMP_roop]=YR;
     ZMP_SIDE_YLL[ZMP_roop]=YL;

     }
     else{
         YL=_CYL1*exp(sqrt(GRAV/CENT_G)*TIME)+_CYL2*exp(-sqrt(GRAV/CENT_G)*TIME);
         YR=_CRf21+_CRf22*(TIME-period/4)+_CRf23*(TIME-period/4)*(TIME-period/4)+_CRf24*(TIME-period/4)*(TIME-period/4)*(TIME-period/4);

         ZMP_SIDE_YRR[ZMP_roop]=YR;
         ZMP_SIDE_YLL[ZMP_roop]=YL;
     }
}
else if(TIME>period/2){

	TIME=TIME-period/2;
	//---------------------------------------after---------------------------------------
if(TIME<=period/4){
     YR=_CYR1*exp(sqrt(GRAV/CENT_G)*(TIME))+_CYR2*exp(-sqrt(GRAV/CENT_G)*(TIME));
     YL=_CLf11+_CLf12*(TIME)+_CLf13*(TIME)*(TIME)+_CLf14*(TIME)*(TIME)*(TIME);

     ZMP_SIDE_YRR[ZMP_roop]=YR;
     ZMP_SIDE_YLL[ZMP_roop]=YL;

}
else{
    YR=_CYR1*exp(sqrt(GRAV/CENT_G)*(TIME))+_CYR2*exp(-sqrt(GRAV/CENT_G)*(TIME));
    YL=_CLf21+_CLf22*(TIME-period/4)+_CLf23*(TIME-period/4)*(TIME-period/4)+_CLf24*(TIME-period/4)*(TIME-period/4)*(TIME-period/4);

    ZMP_SIDE_YRR[ZMP_roop]=YR;
    ZMP_SIDE_YLL[ZMP_roop]=YL;
}
}
else;
}//ZMP_SIDE終了

void ZMP_FORWARD(double TIME){//ロボットの正面に対して前後方向のZMP座標計算Y
	double XL,XR,ZL,YR;
    double C1,C2,kita;
    double T=period/2,G=T/2,G1=T*3/4,G2=T-G1;

    if(TIME<=period/2){
    	if(TIME<=G1){
    		XL=_CXL1*exp(sqrt(GRAV/CENT_G)*(TIME))+_CXL2*exp(-sqrt(GRAV/CENT_G)*(TIME))+offset;
    		XR=_CRXf11+_CRXf12*(TIME)+_CRXf13*(TIME)*(TIME)+_CRXf14*(TIME)*(TIME)*(TIME)+offset;
    		ZMP_FORWARD_XRR=XR;
    		ZMP_FORWARD_XLL=XL;

    	}
    	else{
    		 XL=_CXL1*exp(sqrt(GRAV/CENT_G)*(TIME))+_CXL2*exp(-sqrt(GRAV/CENT_G)*(TIME))+offset;
    		 TIME=TIME-G1;
   		     XR=_CRXf21+_CRXf22*(TIME)+_CRXf23*(TIME)*(TIME)+_CRXf24*(TIME)*(TIME)*(TIME)+offset;
   		     ZMP_FORWARD_XRR=XR;
		     ZMP_FORWARD_XLL=XL;

    		       	}
    }
    //-----------------------------------------------------------------------------------------
    else{
    	TIME=TIME-T;
    	if(TIME<=G1){
    		XR=_CXR1*exp(sqrt(GRAV/CENT_G)*(TIME))+_CXR2*exp(-sqrt(GRAV/CENT_G)*(TIME))+offset;
    		XL=_CLXf11+_CLXf12*(TIME)+_CLXf13*(TIME)*(TIME)+_CLXf14*(TIME)*(TIME)*(TIME)+offset;
		     ZMP_FORWARD_XRR=XR;
		     ZMP_FORWARD_XLL=XL;

    	}
    	else{

    		XR=_CXR1*exp(sqrt(GRAV/CENT_G)*(TIME))+_CXR2*exp(-sqrt(GRAV/CENT_G)*(TIME))+offset;
    		TIME=TIME-G1;
    		XL=_CLXf21+_CLXf22*(TIME)+_CLXf23*(TIME)*(TIME)+_CLXf24*(TIME)*(TIME)*(TIME)+offset;
  		    ZMP_FORWARD_XRR=XR;
  		    ZMP_FORWARD_XLL=XL;

    		        	}
    }
}//ZMP_FORWARD終了

void ZMP_UP(double TIME){//ロボットの正面に対し上下方向のZMP座標計算Z

double ZR,ZL;

double T=period/2;

if(TIME<=T){
	if(TIME<=T/4){
	     ZL=WALK_HEIGHT;//+(asiage/3)*sin(2*pi/period*TIME);
	     ZR=WALK_HEIGHT-(CZ3*TIME*TIME+CZ4*TIME*TIME*TIME);
	     ZMP_SIDE_ZRR[ZMP_roop]=ZR;
	     ZMP_SIDE_ZLL[ZMP_roop]=ZL;
	}
	else if(TIME<T*3/4){
	     ZL=WALK_HEIGHT;//+(asiage/3)*sin(2*pi/period*TIME);
	     ZR=WALK_HEIGHT-(asiage)*sin(2*pi/period*TIME);
	     ZMP_SIDE_ZRR[ZMP_roop]=ZR;
	     ZMP_SIDE_ZLL[ZMP_roop]=ZL;
	}
	else{
		 TIME=TIME-T*3/4;
	     ZL=WALK_HEIGHT;//+(asiage/3)*sin(2*pi/period*TIME);
	     ZR=WALK_HEIGHT-(CZ3*(T/4-TIME)*(T/4-TIME)+CZ4*(T/4-TIME)*(T/4-TIME)*(T/4-TIME));
	     ZMP_SIDE_ZRR[ZMP_roop]=ZR;
	     ZMP_SIDE_ZLL[ZMP_roop]=ZL;
	}
}
else{
	TIME=TIME-period/2;
	if(TIME<=T/4){
	    ZR=WALK_HEIGHT;//+(asiage/3)*sin(2*pi/period*TIME);
	    ZL=WALK_HEIGHT-(CZ3*TIME*TIME+CZ4*TIME*TIME*TIME);
	    ZMP_SIDE_ZRR[ZMP_roop]=ZR;
	    ZMP_SIDE_ZLL[ZMP_roop]=ZL;
	}
	else if(TIME<=T*3/4){
	    ZR=WALK_HEIGHT;//(asiage/3)*sin(2*pi/period*(TIME-period/2));
	    ZL=WALK_HEIGHT-(asiage)*sin(2*pi/period*(TIME));
	    ZMP_SIDE_ZRR[ZMP_roop]=ZR;
	    ZMP_SIDE_ZLL[ZMP_roop]=ZL;
	}
	else{
		TIME=TIME-T*3/4;
	     ZR=WALK_HEIGHT;//+(asiage/3)*sin(2*pi/period*TIME);
	     ZL=WALK_HEIGHT-(CZ3*(T/4-TIME)*(T/4-TIME)+CZ4*(T/4-TIME)*(T/4-TIME)*(T/4-TIME));
	    ZMP_SIDE_ZRR[ZMP_roop]=ZR;
	    ZMP_SIDE_ZLL[ZMP_roop]=ZL;
	}
}
//　歩行計算用プログラム継続用
}
//-----------------------------通常歩行座標決定用プログラム終了-------------------------------------------------

// ----------------------------歩行はじめ座標決定用プログラム終了----------------------------------------------------
void ZMP_SIDE_START(double TIME){//ロボットの正面に対し左右方向のZMP座標計算Y
double YL,YR,ZR,ZL;
double T=period/2;

if(TIME<T){
	if(TIME<T/2){
     YL=_CYL1*exp(sqrt(GRAV/CENT_G)*TIME)+_CYL2*exp(-sqrt(GRAV/CENT_G)*TIME);
     YR=Ymax+(Ymax-YL);
     ZL=WALK_HEIGHT;
     ZR=WALK_HEIGHT;
     ZMP_SIDE_YRR[ZMP_roop]=YR;
     ZMP_SIDE_YLL[ZMP_roop]=YL;
     ZMP_SIDE_ZRR[ZMP_roop]=ZR;
     ZMP_SIDE_ZLL[ZMP_roop]=ZL;

	}
	else if(TIME<T/2+T/4){
	     YL=_CYL1*exp(sqrt(GRAV/CENT_G)*TIME)+_CYL2*exp(-sqrt(GRAV/CENT_G)*TIME);
	     TIME=TIME-T/2;
	     YR=_CRf11+_CRf12*(TIME)+_CRf13*(TIME)*(TIME)+_CRf14*(TIME)*(TIME)*(TIME);
	     ZMP_SIDE_YRR[ZMP_roop]=YR;
	     ZMP_SIDE_YLL[ZMP_roop]=YL;
                 }
	else{

	     YL=_CYL1*exp(sqrt(GRAV/CENT_G)*TIME)+_CYL2*exp(-sqrt(GRAV/CENT_G)*TIME);
	     TIME=TIME-T/2-T/4;
	     YR=_CRf21+_CRf22*(TIME)+_CRf23*(TIME)*(TIME)+_CRf24*(TIME)*(TIME)*(TIME);

	     ZMP_SIDE_YRR[ZMP_roop]=YR;
	     ZMP_SIDE_YLL[ZMP_roop]=YL;
                 }

	}
//-------------------------------------------------------------------
else{
     TIME=TIME-T;
     if(TIME<T/2){
     YR=_CYR1*exp(sqrt(GRAV/CENT_G)*(TIME))+_CYR2*exp(-sqrt(GRAV/CENT_G)*(TIME));
     YL=_CLf11+_CLf12*(TIME)+_CLf13*(TIME)*(TIME)+_CLf14*(TIME)*(TIME)*(TIME);
     ZMP_SIDE_YRR[ZMP_roop]=YR;
     ZMP_SIDE_YLL[ZMP_roop]=YL;
     }
     else{
         YR=_CYR1*exp(sqrt(GRAV/CENT_G)*(TIME))+_CYR2*exp(-sqrt(GRAV/CENT_G)*(TIME));
         TIME=TIME-T/2;
         YL=_CLf21+_CLf22*(TIME)+_CLf23*(TIME)*(TIME)+_CLf24*(TIME)*(TIME)*(TIME);
         ZMP_SIDE_YRR[ZMP_roop]=YR;
         ZMP_SIDE_YLL[ZMP_roop]=YL;
     }
}

}

void ZMP_FORWARD_START(double TIME){//ロボットの正面に対し前後方向のZMP座標計算X
	double XL,XR,ZL,YR;
    double T=period/2,G=T/2,G1=T*3/4,G2=T-G1;
//        pc.printf("%f\n\r",TIME);
        if(TIME<=period/2){
        	if(TIME<=T/2){
        	    XL=offset;
        		XR=offset;
        		ZMP_FORWARD_XRR=XR;
        		ZMP_FORWARD_XLL=XL;}
        	else{
        		TIME=TIME-T/2;
        		if(TIME<=T/4){
        		     XR=_CRXf11+_CRXf12*(TIME)+_CRXf13*(TIME)*(TIME)+_CRXf14*(TIME)*(TIME)*(TIME)+offset;
        		     XL=offset;
        		     ZMP_FORWARD_XRR=XR;
        		     ZMP_FORWARD_XLL=XL;
//        		     pc.printf("%f\n\r",_CRXf13);
        		}
        		else{
        			TIME=TIME-T/4;
       		     XR=_CRXf21+_CRXf22*(TIME)+_CRXf23*(TIME)*(TIME)+_CRXf24*(TIME)*(TIME)*(TIME)+offset;
       		     XL=offset;
    		     ZMP_FORWARD_XRR=XR;
    		     ZMP_FORWARD_XLL=XL;
        		}
           	}
        }
        //-----------------------------------------------------------------------------------------
        else{
        	TIME=TIME-T;
        	if(TIME<=G1){
        		XR=_CXR1*exp(sqrt(GRAV/CENT_G)*(TIME))+_CXR2*exp(-sqrt(GRAV/CENT_G)*(TIME))+offset;
        		XL=_CLXf11+_CLXf12*(TIME)+_CLXf13*(TIME)*(TIME)+_CLXf14*(TIME)*(TIME)*(TIME)+offset;
   		     ZMP_FORWARD_XRR=XR;
   		     ZMP_FORWARD_XLL=XL;
        	}
        	else{
        		XR=_CXR1*exp(sqrt(GRAV/CENT_G)*(TIME))+_CXR2*exp(-sqrt(GRAV/CENT_G)*(TIME))+offset;
        		TIME=TIME-G1;
        		XL=_CLXf21+_CLXf22*(TIME)+_CLXf23*(TIME)*(TIME)+_CLXf24*(TIME)*(TIME)*(TIME)+offset;
      		    ZMP_FORWARD_XRR=XR;
      		    ZMP_FORWARD_XLL=XL;
        		        	}
        }
}

void ZMP_UP_START(double TIME){//ロボットの正面に対し上下方向のZMP座標計算X

double ZR,ZL;
double T=period/2;
double G=T/2;

if(TIME<period/2){
	if(TIME<=T/2){
	     ZL=WALK_HEIGHT;//+(asiage/3)*sin(2*pi/period*TIME);
	     ZR=WALK_HEIGHT;
	     ZMP_SIDE_ZRR[ZMP_roop]=ZR;
	     ZMP_SIDE_ZLL[ZMP_roop]=ZL;
	}
	else if(TIME<T/2+G/4){
		 TIME=TIME-T/2;
	     ZL=WALK_HEIGHT;//+(asiage/3)*sin(2*pi/period*TIME);
	     ZR=WALK_HEIGHT-(CZE3*(TIME)*(TIME)+CZE4*(TIME)*(TIME)*(TIME));
	     ZMP_SIDE_ZRR[ZMP_roop]=ZR;
	     ZMP_SIDE_ZLL[ZMP_roop]=ZL;
	}
	else if(TIME<T/2+G*3/4){
		TIME=TIME-T/2;
	     ZL=WALK_HEIGHT;//+(asiage/3)*sin(2*pi/period*TIME);
	     ZR=WALK_HEIGHT-(asiage)*sin(4*pi/period*TIME);
	     ZMP_SIDE_ZRR[ZMP_roop]=ZR;
	     ZMP_SIDE_ZLL[ZMP_roop]=ZL;
	}
	else{
		 TIME=TIME-T/2-G*3/4;
	     ZL=WALK_HEIGHT;//+(asiage/3)*sin(2*pi/period*TIME);
	     ZR=WALK_HEIGHT-(CZE3*(G/4-TIME)*(G/4-TIME)+CZE4*(G/4-TIME)*(G/4-TIME)*(G/4-TIME));
	     ZMP_SIDE_ZRR[ZMP_roop]=ZR;
	     ZMP_SIDE_ZLL[ZMP_roop]=ZL;
	}
}
else{
	TIME=TIME-period/2;
	if(TIME<=T/4){
	    ZR=WALK_HEIGHT;//+(asiage/3)*sin(2*pi/period*TIME);
	    ZL=WALK_HEIGHT-(CZ3*TIME*TIME+CZ4*TIME*TIME*TIME);
	    ZMP_SIDE_ZRR[ZMP_roop]=ZR;
	    ZMP_SIDE_ZLL[ZMP_roop]=ZL;
	}
	else if(TIME<=T*3/4){
	    ZR=WALK_HEIGHT;//(asiage/3)*sin(2*pi/period*(TIME-period/2));
	    ZL=WALK_HEIGHT-(asiage)*sin(2*pi/period*(TIME));
	    ZMP_SIDE_ZRR[ZMP_roop]=ZR;
	    ZMP_SIDE_ZLL[ZMP_roop]=ZL;
	}
	else{
		TIME=TIME-T*3/4;
	     ZR=WALK_HEIGHT;//+(asiage/3)*sin(2*pi/period*TIME);
	     ZL=WALK_HEIGHT-(CZ3*(T/4-TIME)*(T/4-TIME)+CZ4*(T/4-TIME)*(T/4-TIME)*(T/4-TIME));
	    ZMP_SIDE_ZRR[ZMP_roop]=ZR;
	    ZMP_SIDE_ZLL[ZMP_roop]=ZL;
	}
}
}
/*--------------------------------------
 * 目的
 * 引数		サーボのID（1~127）
 * 返り値
 * 留意点
 *---------------------------------------*/
void ZMP_SIDE_END(double TIME){//ロボットの正面に対し左右方向のZMP座標計算Y

	double YL,YR,ZR,ZL;
    double 	T=period/2;
		//-----------------------start--------------------------------
	     if(TIME<=T/4){
	     YL=_CYL1*exp(sqrt(GRAV/CENT_G)*TIME)+_CYL2*exp(-sqrt(GRAV/CENT_G)*TIME);
	     YR=_CRf11+_CRf12*(TIME)+_CRf13*(TIME)*(TIME)+_CRf14*(TIME)*(TIME)*(TIME);

	     ZMP_SIDE_YRR[ZMP_roop]=YR;
	     ZMP_SIDE_YLL[ZMP_roop]=YL;

	     }
	     else{
	         YL=_CYL1*exp(sqrt(GRAV/CENT_G)*TIME)+_CYL2*exp(-sqrt(GRAV/CENT_G)*TIME);
	         TIME=TIME-T/4;
	         YR=_CRf21+_CRf22*(TIME)+_CRf23*(TIME)*(TIME)+_CRf24*(TIME)*(TIME)*(TIME);

	         ZMP_SIDE_YRR[ZMP_roop]=YR;
	         ZMP_SIDE_YLL[ZMP_roop]=YL;
	     }
}

void ZMP_FORWARD_END(double TIME){//ロボットの正面に対し前後方向のZMP座標計算X
	double XL,XR,ZL,YR;
    double C1,C2,kita;
    double T=period/2,G=T/2,G1=T*3/4,G2=T-G1;
    G1=G1/2;
    G2=G2/2;
    	if(TIME<=G){
    		XL=_CXL1*exp(sqrt(GRAV/CENT_G)*(TIME))+_CXL2*exp(-sqrt(GRAV/CENT_G)*(TIME))+offset;
    		XR=_CRXf11+_CRXf12*(TIME)+_CRXf13*(TIME)*(TIME)+_CRXf14*(TIME)*(TIME)*(TIME)+offset;
    		ZMP_FORWARD_XRR=XR;
    		ZMP_FORWARD_XLL=XL;
    	}
    	else;
}


void ZMP_UP_END(double TIME){//ロボットの正面に対し上下方向のZMP座標計算X
	double ZR,ZL;
	double T=period/4;


		if(TIME<=T/4){
		     ZL=WALK_HEIGHT;//+(asiage/3)*sin(2*pi/period*TIME);
		     ZR=WALK_HEIGHT-(CZ3*TIME*TIME+CZ4*TIME*TIME*TIME);
		     ZMP_SIDE_ZRR[ZMP_roop]=ZR;
		     ZMP_SIDE_ZLL[ZMP_roop]=ZL;
		}
		else if(TIME<T*3/4){
		     ZL=WALK_HEIGHT;//+(asiage/3)*sin(2*pi/period*TIME);
		     ZR=WALK_HEIGHT-(asiage)*sin(pi/T*TIME);
		     ZMP_SIDE_ZRR[ZMP_roop]=ZR;
		     ZMP_SIDE_ZLL[ZMP_roop]=ZL;
		}
		else{
			 TIME=TIME-T*3/4;
		     ZL=WALK_HEIGHT;//+(asiage/3)*sin(2*pi/period*TIME);
		     ZR=WALK_HEIGHT-(CZ3*(T/4-TIME)*(T/4-TIME)+CZ4*(T/4-TIME)*(T/4-TIME)*(T/4-TIME));
		     ZMP_SIDE_ZRR[ZMP_roop]=ZR;
		     ZMP_SIDE_ZLL[ZMP_roop]=ZL;
		}

}
//-------------------------------------歩行終了用座標決定プログラム終了-----------------------------------------------

//-------------------------------------歩行メインプログラム-----------------------------------------------
double buf_Yrate;
void walk(double Xrate,double Yrate){

    if(buf_Yrate>0.3 && Yrate<-0.3){
    	QU=1;
    }
    else{QU=0;}

	    hohaba=hohabaMAX*Yrate;
    	sYmax2=walk_Ymax+6*Xrate;
    	aYmax2=walk_Ymax-6*Xrate;
    	cal_ZMP_SIDE();//ZMP計算用係数決定用C1exp+C2expのC1とC2の決定
//-----------------------------------------------------------------------------------
    	MOVE_TIME.stop();//タイマスタート
    	MOVE_TIME.reset();
    	MOVE_TIME.start();
//  歩行スタート
	while(MOVE_TIME.read()<period){

        if(walk_roop==0){//歩行開始判定
		ZMP_SIDE_START(MOVE_TIME.read());//歩行スタート横方向計算
		ZMP_FORWARD_START(MOVE_TIME.read());//歩行スタート前後方向計算
		ZMP_UP_START(MOVE_TIME.read());//歩行スタート上下向計算

		kikagaku2(ZMP_SIDE_YLL[ZMP_roop],ZMP_SIDE_ZLL[ZMP_roop],ZMP_SIDE_YRR[ZMP_roop],ZMP_SIDE_ZRR[ZMP_roop]);//XYZの座標を角度に変換
		kikagaku(ZMP_FORWARD_XLL,RL[ZMP_roop],ZMP_FORWARD_XRR,RR[ZMP_roop]);//XYZの座標を角度に変換
		convert_radian_to(SITAL1[1],SITAL2[1],SITAL3[1],SITAL4[1],SITAR1[1],SITAR2[1],SITAR3[1],SITAR4[1]);//角度をサーボの値に変換送信

//		pc.printf("%d\n\r",walk_roop);

        }
        else{

        ZMP_SIDE(MOVE_TIME.read());//歩行横方向計算
        ZMP_FORWARD(MOVE_TIME.read());//歩行前後方向計算
        ZMP_UP(MOVE_TIME.read());//歩行上下向計算
		kikagaku2(ZMP_SIDE_YLL[ZMP_roop],ZMP_SIDE_ZLL[ZMP_roop],ZMP_SIDE_YRR[ZMP_roop],ZMP_SIDE_ZRR[ZMP_roop]);//XYZの座標を角度に変換
		kikagaku(ZMP_FORWARD_XLL,RL[ZMP_roop],ZMP_FORWARD_XRR,RR[ZMP_roop]);//XYZの座標を角度に変換
		convert_radian_to(SITAL1[1],SITAL2[1],SITAL3[1],SITAL4[1],SITAR1[1],SITAR2[1],SITAR3[1],SITAR4[1]);//角度をサーボの値に変換送信
//		pc.printf("%f\t",ZMP_SIDE_YLL[ZMP_roop]);
		setPosition(0,0,home_pos[0][0]-int((ZMP_FORWARD_XRR-offset)*30));//手ふり
		setPosition(1,0,home_pos[1][0]+int((ZMP_FORWARD_XLL-offset)*30));//手ふり

//			setPosition(0,1,home_pos[0][1]-int(center_Ymax*50));
//			setPosition(1,1,home_pos[1][1]-int(center_Ymax*50));
        }
	}

	hohaba_a=hohaba;

	MOVE_TIME.reset();
	MOVE_TIME.stop();
	if(walk_roop==0){
	walk_roop=1;
	}
	else;

	buf_Yrate=Yrate;
}

//-------------------------------------歩行メインプログラム-----------------------------------------------
//　歩行終了用プログラム
void walk_reset(){
    double center_Ymax=0;
	if(walk_roop==1){//歩行終了判定
		walk_roop=0;
		cal_ZMP_SIDE_END();
		cal_ZMP_FORWARD_END();
		MOVE_TIME.stop();
		MOVE_TIME.reset();
		MOVE_TIME.start();
		while(MOVE_TIME.read()<period){
	        ZMP_SIDE(MOVE_TIME.read());//歩行終了前横向計算
	        ZMP_FORWARD(MOVE_TIME.read());//歩行終了前前後方向計算
	        ZMP_UP(MOVE_TIME.read());//歩行終了前上下方向計算
			kikagaku2(ZMP_SIDE_YLL[ZMP_roop],ZMP_SIDE_ZLL[ZMP_roop],ZMP_SIDE_YRR[ZMP_roop],ZMP_SIDE_ZRR[ZMP_roop]);//XYZの座標を角度に変換
			kikagaku(ZMP_FORWARD_XLL,RL[ZMP_roop],ZMP_FORWARD_XRR,RR[ZMP_roop]);//XYZの座標を角度に変換
			convert_radian_to(SITAL1[1],SITAL2[1],SITAL3[1],SITAL4[1],SITAR1[1],SITAR2[1],SITAR3[1],SITAR4[1]);//角度をサーボの値に変換送信
			setPosition(0,0,home_pos[0][0]-int((ZMP_FORWARD_XRR-offset)*50));//手ふり
			setPosition(1,0,home_pos[1][0]+int((ZMP_FORWARD_XLL-offset)*50));//手ふり

		}

		MOVE_TIME.stop();
		MOVE_TIME.reset();
		cal_ZMP_SIDE_END2();
		cal_ZMP_FORWARD_END2();
		cal_ZMP_UP_END();
		MOVE_TIME.start();

		while(MOVE_TIME.read()<period/4){

	        ZMP_SIDE_END(MOVE_TIME.read());//歩行終了横向計算
	        ZMP_FORWARD_END(MOVE_TIME.read());//歩行終了前後方向計算
	        ZMP_UP_END(MOVE_TIME.read());//歩行終了上下方向計算
			kikagaku2(ZMP_SIDE_YLL[ZMP_roop],ZMP_SIDE_ZLL[ZMP_roop],ZMP_SIDE_YRR[ZMP_roop],ZMP_SIDE_ZRR[ZMP_roop]);//XYZの座標を角度に変換
			kikagaku(ZMP_FORWARD_XLL,RL[ZMP_roop],ZMP_FORWARD_XRR,RR[ZMP_roop]);//XYZの座標を角度に変換
			convert_radian_to(SITAL1[1],SITAL2[1],SITAL3[1],SITAL4[1],SITAR1[1],SITAR2[1],SITAR3[1],SITAR4[1]);//角度をサーボの値に変換送信
			setPosition(0,0,home_pos[0][0]-int((ZMP_FORWARD_XRR-offset)*100));//手ふり
			setPosition(1,0,home_pos[1][0]+int((ZMP_FORWARD_XLL-offset)*100));//手ふり
		}

		MOVE_TIME.stop();
		MOVE_TIME.reset();
		MOVE_TIME.start();

		hohaba=0;
		hohaba2=0;
		hohaba_a=0;
		    aYmax=Ymax;
		    sYmax=Ymax;
		    aYmax2=Ymax;
		    sYmax2=Ymax;
		    period=buf_period;
		}

	else;
}
//歩行終了用プログラム

// サーボ角度データ送信用プログラム
void convert_radian_to(double sitaL1,double sitaL2,double sitaL3,double sitaL4,
		double sitaR1,double sitaR2,double sitaR3,double sitaR4){
    int roop;
	double sitaL[6];
	double sitaR[6];
	double valueL[6];
	double valueR[6];
	int SET_VALUE_RIGT[6];
	int SET_VALUE_LEFT[6];
	//cal_to sita
	sitaL[1]=sitaL1*1697.652749; sitaL[2]=sitaL2*1697.652749; sitaL[3]=sitaL3*1697.652749; sitaL[4]=sitaL4*1697.652749;
//	sitaL[5]=sitaL5*1697.652749;
	sitaR[1]=sitaR1*1697.652749; sitaR[2]=sitaR2*1697.652749; sitaR[3]=sitaR3*1697.652749; sitaR[4]=sitaR4*1697.652749;
//	sitaR[5]=sitaR5*1697.652749;
	//cal to sita_en
	//cal_to val
	valueL[1]=sitaL[1]+home_pos[3][1];valueL[2]=sitaL[2]+home_pos[3][2];valueL[3]=+sitaL[3]+home_pos[3][3];valueL[4]=sitaL[4]+home_pos[3][4];
	valueR[1]=-sitaR[1]+home_pos[2][0];valueR[2]=-sitaR[2]+home_pos[2][1];valueR[3]=+sitaR[3]+home_pos[2][2];valueR[4]=-sitaR[4]+home_pos[2][3];
    //cal val en
	for(roop=1;roop<5;roop++){
		SET_VALUE_RIGT[roop] = int(valueR[roop]);
		SET_VALUE_LEFT[roop] = int(valueL[roop]);
	}

	//SET_VALUE TO SAVO RIGHT
	for(roop=1;roop<5;roop++){
		setPosition(2,roop-1,SET_VALUE_RIGT[roop]);
		now_pos[2][roop-1]=SET_VALUE_RIGT[roop];
//		wait(1);
	}
	//SET_VALUE TO SAVO LEFT
	for(roop=1;roop<5;roop++){
		setPosition(3,roop,SET_VALUE_LEFT[roop]);
		now_pos[3][roop]=SET_VALUE_LEFT[roop];
//		wait(1);
	}
}
// サーボ角度データ送信用プログラム終了


void cal_ZMP_SIDE(){
    int roop;
    double sig=0;

    double ST=sqrt( GRAV/CENT_G );
    double T=period/2;
	//----------------------------------------decide-YL-YR--------------------------------
	    if(walk_roop==0){
         sYmax=Ymax;
         aYmax=Ymax;
	//----------------------------------cal_YMAX---------------------------------------------------------------
	//周期/２までの動きとそこからの動きの考察。左足はノーマル右足がイレギュラー。
	//周期/4まで右足は左足と同期。そこから足を上げながら目標値へ急ぐ。
	    	double _Ca_point_TE,_Ca_speed_TE;
	    	sYmax=sYmax2;
	    	aYmax=aYmax2;
	        _CYL1= (aYmax-Ymax*exp(-ST*T))/(exp(ST*T)-exp(-ST*T));
	        _CYL2=Ymax-_CYL1;
	        _Cs_point_T=_CYL1*exp(ST*T)+_CYL2*exp(-ST*T);//CSのt=T/2における位置
	        _Cs_speed_T=_CYL1*ST*exp(ST*T)-_CYL2*ST*exp(-ST*T);//CSのt=T/2における速さ

	        _Ca_point_TE=2*Ymax-(_CYL1*exp(ST*T/2)+_CYL2*exp(-ST*T/2));//CSのt=T/2における位置
	        _Ca_speed_TE=-(_CYL1*ST*exp(ST*T/2)-_CYL2*ST*exp(-ST*T/2));//CSのt=T/2における速さ

	        _CYR1=(sYmax-_Cs_speed_T/ST*exp(-ST*T))/(exp(ST*T)+exp(-ST*T));//-CS_speed_TからのaYmax2で動く。
	        _CYR2=_CYR1+_Cs_speed_T/ST;
	        _Ca_point_0=_CYR1+_CYR2;
	        _Ca_speed_0=_CYR1*ST-_CYR2*ST;
	        _Ca_point_T=_CYR1*exp(ST*T)+_CYR2*exp(-ST*T);
	        _Ca_speed_T=_CYR1*ST*exp(ST*T)-_CYR2*ST*exp(-ST*T);
	        //---------------------------------------------------------------------------------------
	        // next_walk_PP
	        Vsn=_CYR1*ST*exp(ST*period/2)-_CYR2*ST*exp(-ST*period/2);
	        _CYL1n=(aYmax-_Ca_speed_T/ST*exp(-ST*T))/(exp(ST*T)+exp(-ST*T));//sYmaxで動く逆の足はaYmax2の動きを目指す。
	        _CYL2n=_CYR1+_Ca_speed_T/ST;
	        _Csn_point_0=_CYL1n+_CYL2n;//CSnのt=0における位置
	        _Csn_speed_0=_CYL1n*ST-_CYL2n*ST;//CSnのt=0における速さ
	        // next_walk_PP_end
	        //------------------------------------------------------------------------------------------
	    	//-------------------------------------ここから浮いている足についての考察-------------------------------
	    	        double Ya_RNU=(_Ca_point_TE+_Ca_point_0)/2*1.1;
	    	        double Ys_LNU=(_Csn_point_0+_Cs_point_T)/2*1.2;
                    double TE=T/2;
	        _CRf11=_Ca_point_TE;//_Ca_point_0と_Ca_speed_0と_Cab_speed_Tと_Cab_speed_Tから動きを決める。
	        _CRf12=_Ca_speed_TE;
	        _CRf13=(3*(Ya_RNU-_Ca_point_TE-_Ca_speed_TE*TE/2)+_Ca_speed_TE*TE/2)/((TE/2)*(TE/2));
	        _CRf14=(-_Ca_speed_TE-2*_CRf13*TE/2)/(3*(TE/2)*(TE/2));
	        _CRf21=Ya_RNU;
	        _CRf22=0;
	        _CRf24=(_Ca_speed_0*T/2-2*(_Ca_point_0-Ya_RNU))/((TE/2)*(TE/2)*(TE/2));
	        _CRf23=(_Ca_point_0-Ya_RNU-_CRf24*((TE/2)*(TE/2)*(TE/2)))/((TE/2)*(TE/2));

	        _CLf11=_Cs_point_T;//_Ca_point_0と_Ca_speed_0と_Cab_speed_Tと_Cab_speed_Tから動きを決める。
	        _CLf12=_Cs_speed_T;
	        _CLf13=(3*(Ys_LNU-_Cs_point_T-_Cs_speed_T*T/2)+_Cs_speed_T*T/2)/((T/2)*(T/2));
	        _CLf14=(-_Cs_speed_T-2*_CLf13*T/2)/(3*(T/2)*(T/2));
	        _CLf21=Ys_LNU;
	        _CLf22=0;
	        _CLf24=(_Csn_speed_0*T/2-2*(_Csn_point_0-Ys_LNU))/((T/2)*(T/2)*(T/2));
	        _CLf23=(_Csn_point_0-Ys_LNU-_CLf24*((T/2)*(T/2)*(T/2)))/((T/2)*(T/2));

	//----------------------CAL_YMAX_END----------------------------------------------------------
	         cal_asiage_START();
	         cal_asiage();
	     	 cal_ZMP_FORWARD_START();


	    }
	    else{
	    	//   save_buf_data
	        Cs1_buf=_CYL1;
	        Cs2_buf=_CYL2;
	        Ca1_buf=_CYR1;
	        Ca2_buf=_CYR2;
	        // save_end
	//--------------------------------------------------------------------------
	        _Cab_speed_T=Ca1_buf*ST*exp(ST*period/2)-Ca2_buf*ST*exp(-ST*period/2);
	        _Cab_point_T=Ca1_buf*exp(ST*period/2)+Ca2_buf*exp(-ST*period/2);
	//--------------------------------------------------------------------------------------
	        sYmax=sYmax2;
	        _CYL1=(aYmax-_Cab_speed_T/ST*exp(-ST*T))/(exp(ST*T)+exp(-ST*T));//-VSからのsYmaxで動く。
	        _CYL2=_CYL1+_Cab_speed_T/ST;
	        _Cs_point_T=_CYL1*exp(ST*T)+_CYL2*exp(-ST*T);//CSのt=T/2における位置
	        _Cs_speed_T=_CYL1*ST*exp(ST*T)-_CYL2*ST*exp(-ST*T);//CSのt=T/2における速さ
	//-----------------------------------------------------------------------------------------------
	        aYmax=aYmax2;
	        _CYR1=(sYmax-_Cs_speed_T/ST*exp(-ST*T))/(exp(ST*T)+exp(-ST*T));//-CS_speed_TからのaYmax2で動く。
	        _CYR2=_CYR1+_Cs_speed_T/ST;
	        _Ca_point_0=_CYR1+_CYR2;//CSのt=0における位置
	        _Ca_speed_0=_CYR1*ST-_CYR2*ST;//CSのt=0における速さ
	        _Ca_point_T=_CYR1*exp(ST*T)+_CYR2*exp(-ST*T);//CSのt=T/2における位置
	        _Ca_speed_T=_CYR1*ST*exp(ST*T)-_CYR2*ST*exp(-ST*T);//CSのt=T/2における速さ
	 //------------------------------------------------------------------------------------------
//	        // next_walk_PP
	        _CYL1n=(aYmax-_Ca_speed_T/ST*exp(-ST*T))/(exp(ST*T)+exp(-ST*T));//-VSからのsYmaxで動く。
	        _CYL2n=_CYL1n+_Ca_speed_T/ST;
	        _Csn_point_0=_CYL1n+_CYL2n;//CSのt=0における位置
	        _Csn_speed_0=_CYL1n*ST-_CYL2n*ST;//CSのt=0における速さ
	        // next_walk_PP_end
	//-------------------------------------ここから浮いている足についての考察-------------------------------
	        double Ya_RNU=(_Cab_point_T+_Ca_point_0)/2*1.4;
	        double Ys_LNU=(_Csn_point_0+_Cs_point_T)/2*1.4;


	        _CRf11=_Cab_point_T;//_Ca_point_0と_Ca_speed_0と_Cab_speed_Tと_Cab_speed_Tから動きを決める。
	        _CRf12=_Cab_speed_T;
	        _CRf13=(3*(Ya_RNU-_Cab_point_T-_Cab_speed_T*T/2)+_Cab_speed_T*T/2)/((T/2)*(T/2));
	        _CRf14=(-_Cab_speed_T-2*_CRf13*T/2)/(3*(T/2)*(T/2));
	        _CRf21=Ya_RNU;
	        _CRf22=0;
	        _CRf24=(_Ca_speed_0*T/2-2*(_Ca_point_0-Ya_RNU))/((T/2)*(T/2)*(T/2));
	        _CRf23=(_Ca_point_0-Ya_RNU-_CRf24*((T/2)*(T/2)*(T/2)))/((T/2)*(T/2));

	        _CLf11=_Cs_point_T;//_Ca_point_0と_Ca_speed_0と_Cab_speed_Tと_Cab_speed_Tから動きを決める。
	        _CLf12=_Cs_speed_T;
	        _CLf13=(3*(Ys_LNU-_Cs_point_T-_Cs_speed_T*T/2)+_Cs_speed_T*T/2)/((T/2)*(T/2));
	        _CLf14=(-_Cs_speed_T-2*_CLf13*T/2)/(3*(T/2)*(T/2));
	        _CLf21=Ys_LNU;
	        _CLf22=0;
	        _CLf24=(_Csn_speed_0*T/2-2*(_Csn_point_0-Ys_LNU))/((T/2)*(T/2)*(T/2));
	        _CLf23=(_Csn_point_0-Ys_LNU-_CLf24*((T/2)*(T/2)*(T/2)))/((T/2)*(T/2));
//	        pc.printf("%f\n\r",_CRf12+2*_CRf13*T/2+3*_CRf14*T/2*T/2);
	        cal_asiage();
	        if(QU==1){
	        n_cal_ZMP_FORWARD();
	        }
	        else{
	        cal_ZMP_FORWARD();
	        }

	    }
}

void cal_asiage(){

	double V,Z;
	double T=period/2;
	double G=T/4;
	V=asiage*pi/T/sqrt(2);
	Z=asiage/sqrt(2);
	CZ1=0;
	CZ2=0;
	CZ4=(V*G-2*Z)/(G*G*G);
	CZ3=(Z-CZ4*(G*G*G))/(G*G);

}
void cal_asiage_START(){
	double V,Z;
	double T=period/4;
	double G=T/4;
	V=asiage*pi/T/sqrt(2);
	Z=asiage/sqrt(2);
	CZE1=0;
	CZE2=0;
	CZE4=(V*G-2*Z)/(G*G*G);
	CZE3=(Z-CZE4*(G*G*G))/(G*G);
//	pc.printf("%f\t",CZE3);
//	pc.printf("%f\n\r",CZE4);

}

void cal_ZMP_FORWARD_START(){

    double T=period/2,G=T/2,G1=T*3/4,G2=T-G1;
    double aX=-hohaba/2;
    double ST=sqrt( GRAV/CENT_G );
    double _CR_point_0,_CR_point_T,_CR_speed_0,_CR_speed_T;
    double _CL_point_T,_CL_speed_T,_CLn_point_0,_CLn_speed_0;
    //-----------左足　t=T/2までX＝０；ｔ＝T/2からｔ＝Tまで-hohaba/2に左足を持っていく。
    //-----------右足　t=T/2までX=0;t=T/2からt=Tまでhohaba/2に持っていく。

    _CXL1=0;
    _CXL2=0;
    _CL_point_T=0;
    _CL_speed_T=0;

    _CXR1=aX/(exp(ST*T)+exp(-ST*T));//-VSからのsYmaxで動く。
    _CXR2=_CXR1;
    _CR_point_0=_CXR1+_CXR2;
    _CR_speed_0=_CXR1*ST-_CXR2*ST;
    _CR_point_T=_CXR1*exp(ST*T)-_CXR2*exp(-ST*T);
    _CR_speed_T=_CXR1*ST*exp(ST*T)-_CXR2*ST*exp(-ST*T);

    _CLn1=(aX+_CR_speed_T/ST*exp(-ST*T))/(exp(ST*T)+exp(-ST*T));
    _CLn2=_CLn1-_CR_speed_T/ST;
    _CLn_point_0=_CLn1+_CLn2;
    _CLn_speed_0=_CLn1*ST-_CLn2*ST;
//    pc.printf("%f\n\r",_CXR1);


    double Ya_RNU=_CR_point_0*1.05;
    double Ys_LNU=_CLn_point_0*2;

    _CRXf11=0;
    _CRXf12=0;
    _CRXf13=(3*(Ya_RNU))/((G/2)*(G/2));
    _CRXf14=(-2*_CRXf13*G/2)/(3*(G/2)*(G/2));
    _CRXf21=Ya_RNU;
    _CRXf22=0;
    _CRXf24=(_CR_speed_0*G/2-2*(_CR_point_0-Ya_RNU))/((G/2)*(G/2)*(G/2));
    _CRXf23=(_CR_point_0-Ya_RNU-_CRXf24*((G/2)*(G/2)*(G/2)))/((G/2)*(G/2));

    _CLXf11=_CL_point_T;//_Ca_point_0と_Ca_speed_0と_Cab_speed_Tと_Cab_speed_Tから動きを決める。
    _CLXf12=_CL_speed_T;
    _CLXf13=(3*(Ys_LNU-_CL_point_T-_CL_speed_T*G1)+_CL_speed_T*G1)/((G1)*(G1));
    _CLXf14=(-_CL_speed_T-2*_CLXf13*G1)/(3*(G1)*(G1));
    _CLXf21=Ys_LNU;
    _CLXf22=0;
    _CLXf24=(_CLn_speed_0*G2-2*(_CLn_point_0-Ys_LNU))/((G2)*(G2)*(G2));
    _CLXf23=(_CLn_point_0-Ys_LNU-_CLXf24*((G2)*(G2)*(G2)))/((G2)*(G2));


}
void cal_ZMP_FORWARD(){

    double T=period/2,G=T/2,G1=T*3/4,G2=T-G1;

    double ST=sqrt( GRAV/CENT_G );
    double _CR_point_0,_CR_point_T,_CR_speed_0,_CR_speed_T;
    double _CL_point_T,_CL_speed_T,_CLn_point_0,_CLn_speed_0;
    double _CRb_speed_T,_CRb_point_T;
    double aX_buf=-hohaba2/2,CL1_buf,CL2_buf,CR1_buf,CR2_buf;
    double aX=-hohaba/2;
    //-----------左足　t=T/2までX＝０；ｔ＝T/2からｔ＝Tまで-hohaba/2に左足を持っていく。
    //-----------右足　t=T/2までX=0;t=T/2からt=Tまでhohaba/2に持っていく。

    CL1_buf=_CXL1;
    CL2_buf=_CXL2;
    CR1_buf=_CXR1;
    CR2_buf=_CXR2;
    _CRb_speed_T=CR1_buf*ST*exp(ST*T)-CR2_buf*ST*exp(-ST*T);
    _CRb_point_T=CR1_buf*exp(ST*T)+CR2_buf*exp(-ST*T);

    _CXL1=(aX_buf+_CRb_speed_T/ST*exp(-ST*T))/(exp(ST*T)+exp(-ST*T));
    _CXL2=_CXL1-_CRb_speed_T/ST;
    _CL_point_T=_CXL1*exp(ST*T)+_CXL2*exp(-ST*T);
    _CL_speed_T=_CXL1*ST*exp(ST*T)-_CXL2*ST*exp(-ST*T);

    _CXR1=(aX+_CL_speed_T/ST*exp(-ST*T))/(exp(ST*T)+exp(-ST*T));//-VSからのsYmaxで動く。
    _CXR2=_CXR1-_CL_speed_T/ST;
    _CR_point_0=_CXR1+_CXR2;
    _CR_speed_0=_CXR1*ST-_CXR2*ST;
    _CR_point_T=_CXR1*exp(ST*T)-_CXR2*exp(-ST*T);
    _CR_speed_T=_CXR1*ST*exp(ST*T)-_CXR2*ST*exp(-ST*T);

    _CLn1=(aX+_CR_speed_T/ST*exp(-ST*T))/(exp(ST*T)+exp(-ST*T));
    _CLn2=_CLn1-_CR_speed_T/ST;
    _CLn_point_0=_CLn1+_CLn2;
    _CLn_speed_0=_CLn1*ST-_CLn2*ST;

    double Ya_RNU=_CR_point_0*1.2;
    double Ys_LNU=_CLn_point_0*1.2;

    _CRXf11=_CRb_point_T;
    _CRXf12=_CRb_speed_T;
    _CRXf13=(3*(Ya_RNU-_CRb_point_T-_CRb_speed_T*G1)+_CRb_speed_T*G1)/((G1)*(G1));
    _CRXf14=(-_CRb_speed_T-2*_CRXf13*G1)/(3*(G1)*(G1));

    _CRXf21=Ya_RNU;
    _CRXf22=0;
    _CRXf24=(_CR_speed_0*G2-2*(_CR_point_0-Ya_RNU))/((G2)*(G2)*(G2));
    _CRXf23=(_CR_point_0-Ya_RNU-_CRXf24*((G2)*(G2)*(G2)))/((G2)*(G2));

    _CLXf11=_CL_point_T;//_Ca_point_0と_Ca_speed_0と_Cab_speed_Tと_Cab_speed_Tから動きを決める。
    _CLXf12=_CL_speed_T;
    _CLXf13=(3*(Ys_LNU-_CL_point_T-_CL_speed_T*G1)+_CL_speed_T*G1)/((G1)*(G1));
    _CLXf14=(-_CL_speed_T-2*_CLXf13*G1)/(3*(G1)*(G1));

    _CLXf21=Ys_LNU;
    _CLXf22=0;
    _CLXf24=(_CLn_speed_0*G2-2*(_CLn_point_0-Ys_LNU))/((G2)*(G2)*(G2));
    _CLXf23=(_CLn_point_0-Ys_LNU-_CLXf24*((G2)*(G2)*(G2)))/((G2)*(G2));
//    pc.printf("%f\n\rpc",_CL_point_T);


    hohaba2=hohaba;

}
//--------------------------------------------停止用プログラム------------------------------------
/*
 * ZMP_SIDEに対しては左足はいつも通り動かす、その後sYmax=Ymaxの方向へ足を動かす。
 *            右足はT=0の時V1,t=TのときV2の方向へ足を動かす。その後足はymaxで待機。
 *            左足はt=TでYmax t=TでV=0としそこでプログラムを終える。合計3/2周期を費やし停止する。
 *
 * ZMP_FORWARDに対しては左足はいつも道理動かす。その後hohaba=0に向かってあしを動かす。
 * 　　　　　　　　　　　右足はV=0に向かって足を動かす。その後hohaba=0に向かって足を動かす。
 * 　　　　　　　　　　　左足はhohaba=0で待機、右足が０にそろうのを待つ。合計3/2周期を費やし停止する。
 */
void cal_ZMP_SIDE_END(){

    double ST=sqrt( GRAV/CENT_G );
    double T=period/2,G=T/2,G1=T*3/4,G2=T-G1;
    double     _CsN_speed_0,_CsN_point_0;
    double _CYL1_N,_CYL2_N;
    aYmax=Ymax;
	//1周期までの動き。
	//   save_buf_data
    Cs1_buf=_CYL1;
    Cs2_buf=_CYL2;
    Ca1_buf=_CYR1;
    Ca2_buf=_CYR2;
    // save_end
//--------------------------------------------------------------------------
    _Cab_speed_T=Ca1_buf*ST*exp(ST*period/2)-Ca2_buf*ST*exp(-ST*period/2);
    _Cab_point_T=Ca1_buf*exp(ST*period/2)+Ca2_buf*exp(-ST*period/2);
//--------------------------------------------------------------------------------------

    _CYL1=(aYmax-_Cab_speed_T/ST*exp(-ST*T))/(exp(ST*T)+exp(-ST*T));//-VSからのsYmaxで動く。
    _CYL2=_CYL1+_Cab_speed_T/ST;
    _Cs_point_T=_CYL1*exp(ST*T)+_CYL2*exp(-ST*T);//CSのt=T/2における位置
    _Cs_speed_T=_CYL1*ST*exp(ST*T)-_CYL2*ST*exp(-ST*T);//CSのt=T/2における速さ
//-----------------------------------------------------------------------------------------------
    _CYL1_N=Ymax/(2*exp(ST*G));
    _CYL2_N=_CYL1_N*exp(2*ST*G);
    _CsN_speed_0=_CYL1_N*ST-_CYL2_N*ST;
    _CsN_point_0=_CYL1_N+_CYL2_N;


	//-----------------------------------------------------------------------------------------------

	        _CYR1=(-_CsN_speed_0+_Cs_speed_T*exp(-ST*T))/(ST*(exp(ST*T)-exp(-ST*T)));//-CS_speed_TからのaYmax2で動く。
	        _CYR2=_CYL1+_Cs_speed_T/ST;
	        _Ca_point_0=_CYR1+_CYR2;//CSのt=0における位置
	        _Ca_speed_0=_CYR1*ST-_CYR2*ST;//CSのt=0における速さ
	        _Ca_point_T=_CYR1*exp(ST*T)+_CYR2*exp(-ST*T);//CSのt=T/2における位置
	        _Ca_speed_T=_CYR1*ST*exp(ST*T)-_CYR2*ST*exp(-ST*T);//CSのt=T/2における速さ
	 //------------------------------------------------------------------------------------------
	        double Ya_RNU=(_Cab_point_T+_Ca_point_0)/2*1.1;
	        double Ys_LNU=_CsN_point_0*1.1;


	        _CRf11=_Cab_point_T;//_Ca_point_0と_Ca_speed_0と_Cab_speed_Tと_Cab_speed_Tから動きを決める。
	        _CRf12=_Cab_speed_T;
	        _CRf13=(3*(Ya_RNU-_Cab_point_T-_Cab_speed_T*T/2)+_Cab_speed_T*T/2)/((T/2)*(T/2));
	        _CRf14=(-_Cab_speed_T-2*_CRf13*T/2)/(3*(T/2)*(T/2));
	        _CRf21=Ya_RNU;
	        _CRf22=0;
	        _CRf24=(_Ca_speed_0*T/2-2*(_Ca_point_0-Ya_RNU))/((T/2)*(T/2)*(T/2));
	        _CRf23=(_Ca_point_0-Ya_RNU-_CRf24*((T/2)*(T/2)*(T/2)))/((T/2)*(T/2));

	        _CLf11=_Cs_point_T;//_Ca_point_0と_Ca_speed_0と_Cab_speed_Tと_Cab_speed_Tから動きを決める。
	        _CLf12=_Cs_speed_T;
	        _CLf13=(3*(Ys_LNU-_Cs_point_T-_Cs_speed_T*T/2)+_Cs_speed_T*T/2)/((T/2)*(T/2));
	        _CLf14=(-_Cs_speed_T-2*_CLf13*T/2)/(3*(T/2)*(T/2));
	        _CLf21=Ys_LNU;
	        _CLf22=0;
	        _CLf24=(_CsN_speed_0*T/2-2*(_CsN_point_0-Ys_LNU))/((T/2)*(T/2)*(T/2));
	        _CLf23=(_CsN_point_0-Ys_LNU-_CLf24*((T/2)*(T/2)*(T/2)))/((T/2)*(T/2));
//	        pc.printf("%f\n\r",_CsN_point_0);

}
void cal_ZMP_SIDE_END2(){
	//１周期以降の動き半歩
    double ST=sqrt( GRAV/CENT_G );
    double T=period/2,G=T/2,G1=G/2,G2=T-G1;
    double     _CsN_speed_0,_CsN_point_0;
    double _CYL1_N,_CYL2_N;

    Cs1_buf=_CYL1;
    Cs2_buf=_CYL2;
    Ca1_buf=_CYR1;
    Ca2_buf=_CYR2;

    _Cab_speed_T=Ca1_buf*ST*exp(ST*T)-Ca2_buf*ST*exp(-ST*T);
    _Cab_point_T=Ca1_buf*exp(ST*T)+Ca2_buf*exp(-ST*T);

    _CYL1=Ymax/(2*exp(ST*G));//-CS_speed_TからのaYmax2で動く。
    _CYL2=_CYL1*exp(2*ST*G);
    _CsN_speed_0=_CYL1*ST-_CYL2*ST;
    _CsN_point_0=_CYL1+_CYL2;



    double Ya_RNU=(Ymax)*1.05;

    _CRf11=_Cab_point_T;//_Ca_point_0と_Ca_speed_0と_Cab_speed_Tと_Cab_speed_Tから動きを決める。
    _CRf12=_Cab_speed_T;
    _CRf13=(3*(Ya_RNU-_Cab_point_T-_Cab_speed_T*G1)+_Cab_speed_T*G1)/((G1)*(G1));
    _CRf14=(-_Cab_speed_T-2*_CRf13*G1)/(3*(G1)*(G1));
    _CRf21=Ya_RNU;
    _CRf22=0;
    _CRf24=(-2*(Ymax-Ya_RNU))/((G1)*(G1)*(G1));
    _CRf23=(Ymax-Ya_RNU-_CRf24*((G1)*(G1)*(G1)))/((G1)*(G1));
//    pc.printf("%f\n\r",_CsN_point_0);
}

void cal_ZMP_FORWARD_END(){
	//1周期までの動き

    double T=period/2,G=T/2,G1=T*3/4,G2=T-G1;

    double ST=sqrt( GRAV/CENT_G );
    double _CR_point_0,_CR_point_T,_CR_speed_0,_CR_speed_T;
    double _CL_point_T,_CL_speed_T,_CLn_point_0,_CLn_speed_0;
    double _CRb_speed_T,_CRb_point_T;
    double aX_buf=-hohaba2/2,CL1_buf,CL2_buf,CR1_buf,CR2_buf;
    double aX=-hohaba/2;
    //-----------左足　t=T/2までX＝０；ｔ＝T/2からｔ＝Tまで-hohaba/2に左足を持っていく。
    //-----------右足　t=T/2までX=0;t=T/2からt=Tまでhohaba/2に持っていく。

    CL1_buf=_CXL1;
    CL2_buf=_CXL2;
    CR1_buf=_CXR1;
    CR2_buf=_CXR2;
    _CRb_speed_T=CR1_buf*ST*exp(ST*T)-CR2_buf*ST*exp(-ST*T);
    _CRb_point_T=CR1_buf*exp(ST*T)+CR2_buf*exp(-ST*T);

    _CXL1=(aX_buf+_CRb_speed_T/ST*exp(-ST*T))/(exp(ST*T)+exp(-ST*T));
    _CXL2=_CXL1-_CRb_speed_T/ST;
    _CL_point_T=_CXL1*exp(ST*T)+_CXL2*exp(-ST*T);
    _CL_speed_T=_CXL1*ST*exp(ST*T)-_CXL2*ST*exp(-ST*T);

    _CXR1=(0-_CL_speed_T*exp(-ST*T))/(ST*(exp(ST*T)-exp(-ST*T)));//-VSからのsYmaxで動く。
    _CXR2=_CXR1-_CL_speed_T/ST;
    _CR_point_0=_CXR1+_CXR2;
    _CR_speed_0=_CXR1*ST-_CXR2*ST;
    _CR_point_T=_CXR1*exp(ST*T)-_CXR2*exp(-ST*T);
    _CR_speed_T=_CXR1*ST*exp(ST*T)-_CXR2*ST*exp(-ST*T);

    _CLn1=0;
    _CLn2=0;
    _CLn_point_0=_CLn1+_CLn2;
    _CLn_speed_0=_CLn1*ST-_CLn2*ST;

    double Ya_RNU=_CR_point_0*1.2;
    double Ys_LNU=_CLn_point_0*1.2;

    _CRXf11=_CRb_point_T;
    _CRXf12=_CRb_speed_T;
    _CRXf13=(3*(Ya_RNU-_CRb_point_T-_CRb_speed_T*G1)+_CRb_speed_T*G1)/((G1)*(G1));
    _CRXf14=(-_CRb_speed_T-2*_CRXf13*G1)/(3*(G1)*(G1));

    _CRXf21=Ya_RNU;
    _CRXf22=0;
    _CRXf24=(_CR_speed_0*G2-2*(_CR_point_0-Ya_RNU))/((G2)*(G2)*(G2));
    _CRXf23=(_CR_point_0-Ya_RNU-_CRXf24*((G2)*(G2)*(G2)))/((G2)*(G2));

    _CLXf11=_CL_point_T;//_Ca_point_0と_Ca_speed_0と_Cab_speed_Tと_Cab_speed_Tから動きを決める。
    _CLXf12=_CL_speed_T;
    _CLXf13=(3*(Ys_LNU-_CL_point_T-_CL_speed_T*G1)+_CL_speed_T*G1)/((G1)*(G1));
    _CLXf14=(-_CL_speed_T-2*_CLXf13*G1)/(3*(G1)*(G1));

    _CLXf21=Ys_LNU;
    _CLXf22=0;
    _CLXf24=(_CLn_speed_0*G2-2*(_CLn_point_0-Ys_LNU))/((G2)*(G2)*(G2));
    _CLXf23=(_CLn_point_0-Ys_LNU-_CLXf24*((G2)*(G2)*(G2)))/((G2)*(G2));
//    pc.printf("%f\n\rpc",_CL_point_T);


    hohaba2=hohaba;

}
void cal_ZMP_FORWARD_END2(){
	//1周期以降の動き半歩。
	//1周期までの動き

    double T=period/2,G=T/2,G1=T*3/4,G2=T-G1;

    double ST=sqrt( GRAV/CENT_G );
    double _CR_point_0,_CR_point_T,_CR_speed_0,_CR_speed_T;
    double _CL_point_T,_CL_speed_T,_CLn_point_0,_CLn_speed_0;
    double _CRb_speed_T,_CRb_point_T;
    double aX_buf=-hohaba2/2,CL1_buf,CL2_buf,CR1_buf,CR2_buf;
    double aX=-hohaba/2;


    CL1_buf=_CXL1;
    CL2_buf=_CXL2;
    CR1_buf=_CXR1;
    CR2_buf=_CXR2;
    _CRb_speed_T=CR1_buf*ST*exp(ST*T)-CR2_buf*ST*exp(-ST*T);
    _CRb_point_T=CR1_buf*exp(ST*T)+CR2_buf*exp(-ST*T);

    _CXL1=0;
    _CXL2=0;
    _CL_point_T=0;
    _CL_speed_T=0;

    double Ya_RNU=0;
//    double Ys_LNU=0;
    _CRXf11=_CRb_point_T;
    _CRXf12=_CRb_speed_T;
    _CRXf13=(3*(Ya_RNU-_CRb_point_T-_CRb_speed_T*G)+_CRb_speed_T*G)/((G)*(G));
    _CRXf14=(-_CRb_speed_T-2*_CRXf13*G)/(3*(G)*(G));

//    _CRXf21=Ya_RNU;
//    _CRXf22=0;
//    _CRXf24=(_CR_speed_0*G2/2-2*(_CR_point_0-Ya_RNU))/((G2/2)*(G2/2)*(G2/2));
//    _CRXf23=(_CR_point_0-Ya_RNU-_CRXf24*((G2/2)*(G2/2)*(G2/2)))/((G2/2)*(G2/2));
    hohaba=0;
    hohaba2=0;

}
//重要　確認　Gの値
void cal_ZMP_UP_END(){

	double V,Z;
	double T=period/4;
	double G=T/4;
	V=asiage*pi/T/sqrt(2);
	Z=asiage/sqrt(2);
	CZ1=0;
	CZ2=0;
	CZ4=(V*G-2*Z)/(G*G*G);
	CZ3=(Z-CZ4*(G*G*G))/(G*G);


}


void n_cal_ZMP_FORWARD(){
	    double T=period/2,G=T/2,G1=T*3/4,G2=T-G1;

	    double ST=sqrt( GRAV/CENT_G );
	    double _CR_point_0,_CR_point_T,_CR_speed_0,_CR_speed_T;
	    double _CL_point_T,_CL_speed_T,_CLn_point_0,_CLn_speed_0;
	    double _CRb_speed_T,_CRb_point_T;
	    double aX_buf=-hohaba2/2,CL1_buf,CL2_buf,CR1_buf,CR2_buf;
	    double aX=-hohaba/2;
	    //-----------左足　t=T/2までX＝０；ｔ＝T/2からｔ＝Tまで-hohaba/2に左足を持っていく。
	    //-----------右足　t=T/2までX=0;t=T/2からt=Tまでhohaba/2に持っていく。

	    CL1_buf=_CXL1;
	    CL2_buf=_CXL2;
	    CR1_buf=_CXR1;
	    CR2_buf=_CXR2;
	    _CRb_speed_T=CR1_buf*ST*exp(ST*T)-CR2_buf*ST*exp(-ST*T);
	    _CRb_point_T=CR1_buf*exp(ST*T)+CR2_buf*exp(-ST*T);

	    _CXL1=(aX_buf+_CRb_speed_T/ST*exp(-ST*T))/(exp(ST*T)+exp(-ST*T));
	    _CXL2=_CXL1-_CRb_speed_T/ST;
	    _CL_point_T=_CXL1*exp(ST*T)+_CXL2*exp(-ST*T);
	    _CL_speed_T=_CXL1*ST*exp(ST*T)-_CXL2*ST*exp(-ST*T);

	    _CXR1=(_CL_speed_T)/(ST*(1-exp(2*ST*G)));//-VSからのsYmaxで動く。
	    _CXR2=_CXR1*exp(2*ST*G);
	    _CR_point_0=_CXR1+_CXR2;
	    _CR_speed_0=_CXR1*ST-_CXR2*ST;
	    _CR_point_T=_CXR1*exp(ST*T)-_CXR2*exp(-ST*T);
	    _CR_speed_T=_CXR1*ST*exp(ST*T)-_CXR2*ST*exp(-ST*T);

	    _CLn1=(aX+_CR_speed_T/ST*exp(-ST*T))/(exp(ST*T)+exp(-ST*T));
	    _CLn2=_CLn1-_CR_speed_T/ST;
	    _CLn_point_0=_CLn1+_CLn2;
	    _CLn_speed_0=_CLn1*ST-_CLn2*ST;

	    double Ya_RNU=_CR_point_0*1.2;
	    double Ys_LNU=_CLn_point_0*1.2;

	    _CRXf11=_CRb_point_T;
	    _CRXf12=_CRb_speed_T;
	    _CRXf13=(3*(Ya_RNU-_CRb_point_T-_CRb_speed_T*G1)+_CRb_speed_T*G1)/((G1)*(G1));
	    _CRXf14=(-_CRb_speed_T-2*_CRXf13*G1)/(3*(G1)*(G1));

	    _CRXf21=Ya_RNU;
	    _CRXf22=0;
	    _CRXf24=(_CR_speed_0*G2-2*(_CR_point_0-Ya_RNU))/((G2)*(G2)*(G2));
	    _CRXf23=(_CR_point_0-Ya_RNU-_CRXf24*((G2)*(G2)*(G2)))/((G2)*(G2));

	    _CLXf11=_CL_point_T;//_Ca_point_0と_Ca_speed_0と_Cab_speed_Tと_Cab_speed_Tから動きを決める。
	    _CLXf12=_CL_speed_T;
	    _CLXf13=(3*(Ys_LNU-_CL_point_T-_CL_speed_T*G1)+_CL_speed_T*G1)/((G1)*(G1));
	    _CLXf14=(-_CL_speed_T-2*_CLXf13*G1)/(3*(G1)*(G1));

	    _CLXf21=Ys_LNU;
	    _CLXf22=0;
	    _CLXf24=(_CLn_speed_0*G2-2*(_CLn_point_0-Ys_LNU))/((G2)*(G2)*(G2));
	    _CLXf23=(_CLn_point_0-Ys_LNU-_CLXf24*((G2)*(G2)*(G2)))/((G2)*(G2));
	//    pc.printf("%f\n\rpc",_CL_point_T);
	    hohaba2=hohaba;
}
