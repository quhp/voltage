/*
 * savo_cont.h
 *
 *  Created on: 2013/05/07
 *      Author: kazuya
 */

#ifndef SAVO_CONT_H_
#define SAVO_CONT_H_
//#include"mbed.h"
//#include"savo_cont.h"
#include"mbed.h"
#include"IOSerial.h"


#define LEFT_ARM 0
#define RIGHT_ARM 1
#define RIGHT_LEG 2
#define LEFT_LEG 3
//-----------------------サーボコントロール---------------------------------------------
void setSpeed(int port,unsigned char id,int sp);
void setPosition(int port,unsigned char id,int pos);
//------------------------サーボコントロール--------------------------------------------

//------------------------モーション作成用--------------------------------------------
void SET_MOTION(int mode,double t,double change,double change2,double change3, int RU0,int RU1, int RU2,int RU3,int RU4,int RU5,
		         int LU0,int LU1, int LU2,int LU3,int LU4, int LU5,
		         int RF0,int RF1, int RF2,int RF3,int RF4, int RF5,
		         int LF0, int LF1, int LF2, int LF3, int LF4, int LF5
		         );//モーション作成用プログラム
void MOVE_SAVO(int mode ,double time,double change_h,double change_f,double cahnge_Y,int RU[],int RF[],int LF[],int LU[]);
//------------------------モーション作成用----------------------------------------------

//void ZMP_FORWARD_GX2(double TIME);いらない
void SET_SAVO();//テスト用
void n_cal_ZMP_FORWARD();//反転用
//double time_cout();いらない
void set_home_pos();
//----------------------------------角度変換　および　サーボの値に変換するプログラム。
double kikagaku(double xL,double yL,double xR,double yR);
double kikagaku2(double xL,double zL,double xR,double zR);//幾何学計算用　kikagaku2の後にkikagakuを使う。
void convert_radian_to(double sitaL1,double sitaL2,double sitaL3,double sitaL4,
		double sitaR1,double sitaR2,double sitaR3,double sitaR4);//ロボットの角度（rad）からサーボのコマンドに変換。
//--------------------------------------------------------------------------
//void obserb_cline_Z();過去の遺物
//double cal_a();
//double cal_b();
//void re_home();
//void sinsa();

//void SET_MOTION(double time, int VALUE[][]);
void walk(double Xrate,double Yrate);//歩行用メインプログラム
void walk_reset();       //ロボットの歩行終了のメインプログラム
//-------------------------ロボットの足の座標計算---------------
void ZMP_SIDE(double TIME);//歩行計算　ロボットの横方向
void ZMP_FORWARD(double TIME);//歩行計算　ロボットの前後方向
void ZMP_UP(double TIME);//歩き終わりの計算　　ロボットの上下
void ZMP_SIDE_START(double TIME);//歩き始めの計算　ロボットの横方向
void ZMP_FORWARD_START(double TIME);//歩き始めの計算　ロボットの前後方向
void ZMP_FORWARD_END(double TIME);// 歩き終わりの計算　　ロボットの前後方向
void ZMP_SIDE_END(double TIME);//歩き終わりの計算　　　ロボットの横方向
//---------------------------------------------------------

//----------------------------- 係数関係-------------------------
void cal_asiage_START();//ロボットの歩き始めの　上下の数式の係数決定
void cal_ZMP_FORWARD_START(); //ロボット歩き始めの 前後の数式の係数決定
void cal_ZMP_FORWARD();       //ロボットの歩行中の前後の係数決定
void cal_ZMP_SIDE();       //ロボットの歩行中の予行方向の係数決定
void cal_asiage();     //ロボットの歩行中の係数決定
void cal_ZMP_FORWARD_END2();  //ロボットの歩行終了時の係数決定
void cal_ZMP_FORWARD_END();   //ロボットの方終了時の係数決定
void cal_ZMP_SIDE_END2();  //ロボットの歩行終了時の係数決定
void cal_ZMP_SIDE_END();   //ロボットの歩行終了時の係数決定
void cal_ZMP_UP_END();   //ロボットの歩行終了時の係数決定
//-----------------------------------------------------------
//void trev();　//モーション
//void adjust_homepos();
//void analize1();
//void analize2();

//void jairo_reset();いらな
//void jump();　モーション
//void jump2();
//void slow_move();
//void Atack_move_savo_GSS(int mode,double time,double change_h,double change_f,int LU[],int RU[],int RF[],int LF[]);
//void punch();//モーション
// void cap_bump();いらない
//void ZMP_FORWARD_G4(double TIME);
//void ZMP_SIDE_GXD(double TIME);
//void walk_cap();     いらない
//void none();
//void ZMP_FORWARD_G3(double TIME);
//void ZMP_FORWARD_G2(double TIME);
//void ZMP_SIDE_GX2(double TIME);
//void last_brust();　いらない
//void cline_mae();
//void cline_usiro();
//int wake_up();モーション
//void wake_up2();モーション
//void connect();モーション
//void test();モーション
void free();//緊急用
//void shagami();モーション
//void modoru();
//void walk_burst();

//void punch();
//void punch_fin(int boil);
//void migi_senkai();
//void hidari_senkai();

//void Atack_move_savo_GX(int mode,double time,double change_h,double change_f,int LU[],int RU[],int RF[],int LF[]);
//void CQC();
//void turn_right();
//void turn_left();
//void matto();
//void nage();
//void back_R();
//void back_L();
//void walk_ART(int mode,double time,double change_h,double change_f,double change_Y,int LU[],int RU[],int RF[],int LF[]);
//void ART_TEST();
//void WALK_ART2(int mode,double time,double change_h,double change_f,double change_Y,int LU[],int RU[],int RF[],int LF[]);
//void  home_mode();
//void WALK_ART3(int mode,double time,double change_h,double change_f,double change_Y,int LU[],int RU[],int RF[],int LF[]);
//void left_punch();
//void right_punch();
//void left_step();
//void right_step();
//void change_speed();
//void star();
//void star2();
//void cal_ZMP_SIDE_Attack_left();
//void ART_MURA();
//void cal_ZMP_SIDE_END2_A();
//void cal_ZMP_SIDE_END_A();
//void kansoku();
#endif /* SAVO_CONT_H_ */
